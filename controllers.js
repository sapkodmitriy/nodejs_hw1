const fs = require("fs");
const path = require("path");

const filesFolder = path.resolve(__dirname, "files");

function createFile(req, res) {
  let { filename, content } = req.body;
  if (!fs.existsSync("./files")) {
    fs.mkdirSync("./files");
  }
  try {
    if (content && filename) {
      const fileNames = fs.readdirSync(filesFolder);
      if (fileNames.includes(filename)) {
        return res.status(400).json({ message: "File already exists" });
      }
      fs.writeFileSync(`./files/${filename}`, content);
      res
        .status(200)
        .json({ message: `File ${filename} created successfully` });
    } else {
      res.status(400).json({ message: "Please specify 'content' parameter" });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: "Server error" });
  }
}

function getFiles(req, res) {
  fs.readdir("./files", (err, files) => {
    if (err) {
      res.status(500).json("Server error");
    } else {
      res.status(200).json({ message: "Success", files });
    }
  });
}

function getFile(req, res) {
  const { filename } = req.params;
  const filePath = path.resolve(filesFolder, filename);
  try {
    fs.readFile(filePath, "UTF-8", (err, data) => {
      if (err) {
        res
          .status(400)
          .json({ message: `No file with '${filename}' filename found` });
      } else {
        res.status(200).json({
          message: "Success",
          filename,
          content: data.toString(),
          extension: path.extname(filename).slice(1),
          uploadedDate: fs.statSync(filePath).mtime,
        });
      }
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: "Server error" });
  }
}

function deleteFile(req, res) {
  const { filename } = req.params;
  fs.unlink(path.resolve(filesFolder, filename), (err) => {
    if (err) {
      res.status(400).json({ message: "Error, no such file" });
    }
    res.status(200).json({ message: `${filename} was successfuly deleted` });
  });
}

function changeFile(req, res) {
  const { filename } = req.params;
  let { content } = req.body;
  try {
    if (content && filename) {
      const fileNames = fs.readdirSync(filesFolder);
      if (!fileNames.includes(filename)) {
        return res
          .status(400)
          .json({ message: `File ${filename} doesn't exist` });
      }
      content =
        path.extname(filename) === ".json" ? JSON.stringify(content) : content;
      fs.writeFileSync(path.resolve(filesFolder, filename), content);
      res.status(200).json({ message: "File content changed successfully" });
    } else {
      res.status(400).json({ message: "Please specify 'content' parameter" });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: "Server error" });
  }
}

module.exports = {
  createFile,
  getFile,
  getFiles,
  deleteFile,
  changeFile,
};
