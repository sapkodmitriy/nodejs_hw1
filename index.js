const express = require("express");
const logger = require("morgan");
const cors = require("cors");
const {
  createFile,
  getFile,
  getFiles,
  deleteFile,
  changeFile,
} = require("./controllers.js");

const PORT = 8080;
const app = express();

app.use(express.json());
app.use(logger("combined"));
app.use(cors());

app.post("/api/files", createFile);
app.get("/api/files", getFiles);
app.get("/api/files/:filename", getFile);
app.delete("/api/files/:filename", deleteFile);
app.put("/api/files/:filename", changeFile);

app.listen(PORT, () => {
  console.log(`Server working on port ${PORT}`);
});
